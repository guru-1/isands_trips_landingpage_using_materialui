import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Collapse } from '@mui/material';


export default function ActionAreaCard({item,checked}) {
  return (
    <Collapse in={checked} {...checked?{timeout:1000}:{}}>
    <Card style={{maxWidth:'840px',  backgroundColor: 'rgba(0,0,0,0.5)', margin:'1rem',overflow:'hidden'}}>
        <CardMedia
          component="img"
          image={item.imageUrl}
          alt="green iguana"
          style={{height:'60vh'}}
        />
        <CardContent style={{backgroundColor:'#fff'}}>
          <Typography gutterBottom variant="h4" component="h1" style={{fontFamily:'Nunito',fontWeight:'bold',color:'blue'}}>
            {item.title}
          </Typography>
          <Typography variant="body2" color="text.secondary" component="h1" style={{fontFamily:'Nunito',color:'green'}}>
          {item.description}
          </Typography>
        </CardContent>
    </Card>
    </Collapse>
  );
}