import React from "react";
import { makeStyles } from "@mui/styles";
import ActionAreaCard from "./imageCard";
import places from "../static/places";
import useWindowPosition from "../static/hook/useWindowlocation";
const useStyles = makeStyles((theme) => ({
  root: {
    minHeight: "100vh",
    display:'flex',
    justifyContent:'space-around',
    flexWrap:'wrap',
  },
}));

const Placestovisit = () => {
  const classes = useStyles();
  const checked=useWindowPosition('header')
  return <div className={classes.root} id='places-to-visit'>
    {places.map((ele)=>(
          <ActionAreaCard item={ele} key={ele.title} checked={checked} />
    ))
    
    }
  </div>;
};

export default Placestovisit;
