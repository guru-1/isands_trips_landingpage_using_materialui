import React, { useState, useEffect } from "react";
import { AppBar, Collapse, IconButton, Toolbar } from "@mui/material";
import SortIcon from "@mui/icons-material/Sort";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import { Link as Scroll } from "react-scroll";

const Header = () => {
  const [checked, setChecked] = useState(false);
  useEffect(() => {
    setChecked(true);
  }, []);
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        fontFamily: "Nunito",
        height: "100vh",
      }}
      id="header"
    >
      <div>
        <AppBar
          style={{
            background: "none",
          }}
          elevation="0"
        >
          <Toolbar style={{ width: "80%", margin: "0 auto" }}>
            <h1 style={{ flexGrow: "1" }}>
              My<span style={{ color: "#5AFF3D" }}>Island.</span>
            </h1>

            <IconButton>
              <SortIcon style={{ color: "white", fontSize: "2.5rem" }} />
            </IconButton>
          </Toolbar>
        </AppBar>
      </div>
      <Collapse in={checked} {...(checked ? { timeout: 1000 } : {})}>
        <div style={{ textAlign: "center", fontSize: "2.75rem" }}>
          <h1 style={{ color: "white" }}>
            Welcome to <br />
            <span style={{ color: "#5AFF3D" }}>My Island.</span>
          </h1>
          <Scroll to="places-to-visit" smooth={true}>
            <KeyboardArrowDownIcon
              style={{ color: "white", fontSize: "5rem" }}
            />
          </Scroll>
        </div>
      </Collapse>
    </div>
  );
};

export default Header;
