import './App.css';
import { makeStyles } from '@mui/styles'
import { CssBaseline } from '@mui/material';
import Header from './components/Header';
import Placestovisit from './components/Placestovisit';

const useStyles = makeStyles(theme => ({
  root: {
    minHeight: '100vh',
    backgroundImage: `url(${process.env.PUBLIC_URL + '/assets/bg.jpg'})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  }
}))
function App() {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Header/>
      <Placestovisit/>
    </div>
  );
}

export default App;
